import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs';
import { element } from 'protractor';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Spinkit } from 'ng-http-loader/spinkits';

import { ListesService } from '../listes.service';
import { List } from '../data-model';

import { ListViewComponent } from '../list-view/list-view.component';


@Component({
  selector: 'lists',
  templateUrl: './lists.component.html',
  styleUrls: ['./lists.component.css']
})
export class ListsComponent implements OnInit {

  lists: any = [];
  public spinkit = Spinkit; //pour le loader pendant le chargement


  constructor(private listService: ListesService,
    private modalService: NgbModal) { } //NgbModal permet d'obtenir la méthode .open pour ouvrir un modal

  ngOnInit() {
    this.get();
  }

  // CRUD en front

  // R
  async get() {   //va chercher les listes existantes
    this.lists = await this.listService.getLists(); //lien avec la promise du getLists
  }

  // C & U / Création et update de listes :
  open(elementlist?: List) {
    const modalRef = this.modalService.open(ListViewComponent);// ouverture du modal (vide)
    if (elementlist) //si liste existante (elementlist en paramètre), ouvre avec le contenu de la liste
    {
      let copyList = JSON.parse(JSON.stringify(elementlist));//stringify transforme l'objet elementlist en string
      //parse remet la string en objet
      //permet de modifier elementlist avant de l'envoyer dans selectedList pour eviter que ngModel modifie en meme temps la liste du modal et celle de la vue
      modalRef.componentInstance.list = copyList; //envoie de la copie de la liste dans l'@Input de listView
    }

    modalRef.result.then(async (resList) => { // promise. création et modification de liste, à la clôture du modal (close dans lisView)
      if (resList) { //resList = liste en retour (param de .close dans listViewComp)
        if (typeof resList.id !== 'undefined') { //vérification de l'existence de la propriété "id" (et non pas sa valeur !!)
          await this.listService.updateList(resList.id, resList); //si existe --> update
          await this.get();
        }
        else {
          await this.listService.addList(resList); //si n'existe pas --> création de la liste
          await this.get();
        }
      }
    }, (reason) => {
    })
      .catch(error => { // Failed
        console.log('error ', error);
      })
      ;
  }

  // D / Effacer la liste
  async delete(elementlist: List) {
    await this.listService.deleteList(elementlist);
    await this.get();

  }
}



