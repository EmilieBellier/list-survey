import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { of } from 'rxjs';
import { Subject } from 'rxjs';

import { List } from './data-model';
import { ListService } from '../providers/models/list.service';

@Injectable()
export class ListesService {

    public listes = []

    constructor(public listService: ListService) { }

    getLists() { // fait appel à ListService (provider/models) pour récupérer les listes dans la base
        return new Promise(async (resolve, reject) => {
            try {
                let lists = await this.listService.find({}) //fonction créée dans lbc model
                if (lists) {
                    resolve(lists)
                }
                else {
                    resolve(null)
                }
            }
            catch (err) {
                reject(err)
            }
        }
        )
    }

    // pour faire le lien avec le survey-modal
    getListByName(paramName) { //fait le lien entre le listName et le paramName
        return new Promise(async (resolve, reject) => {
            try {
                let lists = await this.listService.find({ filter: { where: { listName: `${paramName}` } } })
                if (lists && lists.length == 1) {
                    resolve(lists[0])
                }
                else {
                    resolve(null)
                }
            }
            catch (err) {
                reject(err)
            }
        })
    }

    //fonction de création de liste d'options dans la listes
    addList(liste: List) {
        return new Promise(async (resolve, reject) => {
            try {
                await this.listService.create(liste);
                resolve();
                let name = liste['listName']
                alert(`La liste "${name}" a bien été créée !`)
            }
            catch (err) {
                reject(err)
            }
        })
    }

    // fonction d'update de liste existante
    updateList(id, liste) {
        return new Promise(async (resolve, reject) => {
            try {
                await this.listService.updateById(id, liste)
                resolve();
                let name = liste['listName']
                alert(`La liste "${name}" a bien été modifiée !`)
            }
            catch (err) {
                reject(err)
            }
        })
    }

    //supprime une liste d'options dans listes
    deleteList(id) {
        return new Promise(async (resolve, reject) => {
            let confirmRes = confirm('Etes-vous sûr de vouloir supprimer cette liste ?');
            if (confirmRes === true) {
                try {
                    await this.listService.deleteById(id);

                    resolve();
                }
                catch (err) {
                    reject(err)
                }
            }
        })
    }
}




