//component pour affichage global des questionnaires

import { Component, OnInit, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Spinkit } from 'ng-http-loader/spinkits';
import * as Survey from 'survey-angular';

import { SurveysService } from '../surveys.service';
import { SurveyModalComponent } from '../survey-modal/survey-modal.component';
import { SurveyChoiceModalComponent } from '../survey-choice-modal/survey-choice-modal.component';

@Component({
  selector: 'survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.css']
})
export class SurveyComponent implements OnInit {

  surveyListR: any = []; //Liste des formulaire récupérée via le service dans la collection (Survey)

  public spinkit = Spinkit; //loader

  constructor(private modalService: NgbModal,
    private surveysService: SurveysService) { }

  ngOnInit() { //initialisation par rapport à la liste existante
    this.getListSurv();
  }

  async getListSurv() { //va récupérer les formulaires dans la collection (via le service)
    this.surveyListR = await this.surveysService.getListSurvey();
  }

  async get(param?) { // va chercher le modèle du formulaire survey dans (survey-service)
    if (param) { //récupère celui correspondant au param
      return await this.surveysService.getSurvey(param)
    }
    else { //récupère tous les modèles
      return await this.surveysService.getSurvey()
    }
  }

  update(param) {
    //la fonction get va chercher dans la base le modele qui correspond au param
    this.get(param).then(resSurveyForm => {
      const modalRef = this.modalService.open(SurveyModalComponent);//ouverture de la modal avec composant SurveyModalComponent
      let copySurvey = JSON.parse(JSON.stringify(param));
      modalRef.componentInstance.json = resSurveyForm; //envoi du modèle de formulaire survey (cf survey-service)
      modalRef.componentInstance.contenuCopy = copySurvey; //envoi du contenu (cf list-service)

      modalRef.result.then(async (resSurvey) => { // promise / récupération du contenu du formulaire au close (cf modal / onComplete)
        let oldResSurvey = this.surveyListR.find(res => res['nomForm'] === resSurvey['nomForm']);
        if (resSurvey) {
          if (oldResSurvey) {
            await this.surveysService.updateSurvey(resSurvey); //envoi du contenu récupéré au service (survey) --> surveyListR
            await this.getListSurv(); // pour recharger la vue
          }
        }
      },
        (reason) => {
        })
        .catch(error => { // Failed
          console.log('error ', error);
        });
    })
  }

  add() {
    //la fonction get va chercher tous les modeles dans la base 
    this.get().then(resChoices => {
      const modalChoice = this.modalService.open(SurveyChoiceModalComponent);//ouverture de la modal avec SurveyChoiceModalComponent
      modalChoice.componentInstance.models = resChoices; //envoi du modèle de formulaire survey dans la modal(cf survey-service)

      modalChoice.result.then(async (resChoice) => { // promise / récupération du choix du modele de formulaire au close (cf modal / onComplete)
        const modalRef = this.modalService.open(SurveyModalComponent);
        modalRef.componentInstance.json = resChoice;  //envoi du modèle de formulaire survey a SurveyModalComponent(cf survey-service)

        modalRef.result.then(async (resSurvey) => { // promise / récupération du contenu du formulaire au close (cf modal / onComplete)
          if (resSurvey) {
            await this.surveysService.addSurvey(resSurvey);
            await this.getListSurv();
          }
        }, (reason) => { })
          .catch(error => { // Failed
            console.log('error ', error);
          });

      }, (reason) => { })
        .catch(error => { // Failed
          console.log('error ', error);
        });

    })

  }

  open(elemSurvey?: Object) {
    if (elemSurvey) {//cas update
      this.update(elemSurvey)
    }
    else {//correspond au creation d'un formulaire
      this.add()
    }

  }

  async delete(elementListSurvey) {  //Effacer la liste
    await this.surveysService.deleteSurvey(elementListSurvey.id);
    await this.getListSurv();
  }

}
