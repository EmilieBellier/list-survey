import { Component, OnInit, Input, Output, } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import * as Survey from "survey-angular";
import * as widgets from "surveyjs-widgets";
import { element } from 'protractor';
import { Observable } from 'rxjs';

import { ListesService } from '../../listes/listes.service';
import { List } from '../../listes/data-model';

widgets.icheck(Survey);
widgets.select2(Survey);
widgets.imagepicker(Survey);
widgets.inputmask(Survey);
widgets.jquerybarrating(Survey);
widgets.jqueryuidatepicker(Survey);
widgets.nouislider(Survey);
widgets.select2tagbox(Survey);
widgets.signaturepad(Survey);
widgets.sortablejs(Survey);
widgets.ckeditor(Survey);
widgets.autocomplete(Survey);
widgets.bootstrapslider(Survey);

@Component({
  selector: 'survey-modal',
  templateUrl: './survey-modal.component.html',
  styleUrls: ['./survey-modal.component.css']
})
export class SurveyModalComponent implements OnInit {


  @Input() json: Object; //récupérable avec this.json
  @Input() contenuCopy: any[];

  resForm: Object; //formulaire en { } renvoyé au close du modal
  surveyModel: Survey.Survey; //pour construire le model

  /* Autre façon de styler 
//style des boutons OU avec survey.defaultBootstrapMaterialCss (voir plus bas)
//pensez a ajouter myCss au render de survey
  public myCss = {
    matrix: {
      root: "table table-striped"
    },
    navigationButton: "button btn-lg btn-info right"
  }
*/
  constructor(public activeModal: NgbActiveModal,
    private listsService: ListesService) { }

  ngOnInit() {
    this.createForm().then( //d'abord la structure avec createForm() puis remplissage avec contenu si update
      res => {
        if (this.contenuCopy) { //remplit le formulaire avec le contenu existant si déjà créé
          for (var key in this.contenuCopy) {
            if (key !== "id") {
              this.surveyModel.setValue(key, this.contenuCopy[key]); //setValue remplit avec les valeurs
            }
          }
        }
      })
      .catch(error => { // Failed
        console.log('error ', error);
      })
  }

  async loadElementWithList(element) //pour récupérer les options des listes
  {
    let list: any = await this.listsService.getListByName(element.choicesByUrl['path']) //récupère les listes de list-service (lien entre listName et ChoiccesByUrl) 
    element.choices = [] //réinitialise les options (sinon, le remplit 2 fois)
    if (list && list.options) {
      for (let option of list.options) //boucle sur les options
      {
        element.choices.push({ text: option.optionName, value: option.key }) //push les options récupérées dans le champs "choices"as
      }
    }
  }

  async loadElementImagePickerWithList(element) {
    let list: any = await this.listsService.getListByName(element.choicesByUrl['path'])
    if (list && list.options) {
      element.choices = []
      for (let option of list.options) {
        element.choices.push({ value: option.key, imageLink: option.optionName })
      }
    }
  }

  async createForm() { // Création du formulaire
    for (let page of this.json['pages']) {//création de la structure du formulaire (modèle survey-service)
      for (let element of page['elements']) {
        if (element.type === 'dropdown' || element.type === 'checkbox') {
          await this.loadElementWithList(element) //récupère le contenu des listes de liste-service pour remplir les options
        }
        if (element.type === 'imagepicker') {
          await this.loadElementImagePickerWithList(element);
        }
      }
    }

    //STYLE CSS de Survey (pas possible en fichier .css car css embarqué)
    //CSS ProgressBar - Titre des pages - Bouton - footer
    Survey.defaultBootstrapMaterialCss.progress = "col-12 border border-secondary p-0 mb-3 rounded text-white text-center";
    Survey.defaultBootstrapMaterialCss.pageTitle = "text-info text-center mb-3";
    Survey.defaultBootstrapMaterialCss.progressBar = "progress-bar-info ";
    Survey.defaultBootstrapMaterialCss.question.title = 'mt-4';
    Survey.defaultBootstrapMaterialCss.navigation = { complete: "btn btn-success pull-right", prev: "btn btn-outline-info pull-left", next: "btn btn-outline-info pull-right", start: "" } //ou myCss
    Survey.defaultBootstrapMaterialCss.footer = "pt-5";
    Survey.Survey.cssType = "bootstrapmaterial";

    this.surveyModel = new Survey.Model(this.json); //remplit le modèle du formulaire avec les données récupérées juste avant

    /* Autre façon de changer les noms
   //changement nom des boutons
   //on peut aussi le changer dans le model dans service
   this.surveyModel.pagePrevText = "Précédent";
   this.surveyModel.pageNextText = "Suivant";
   this.surveyModel.completeText = "Envoyer";
   */

    // à la soumission du formulaire
    this.surveyModel.onComplete
      .add(
        (result) => {
          this.resForm = JSON.parse(JSON.stringify(result.data));

          // autre façon de récupérer le form :
          //console.log('surveyModel',this.surveyModel);// contient idem resForm
          // document.querySelector('#surveyResult').innerHTML = "result: " + result.data;
          // document.querySelector('#surveyResult').innerHTML = "result: " + JSON.parse(JSON.stringify(result.data));

          //ajout du champ model de json au resForm
          this.resForm['model'] = this.json['model']
          this.activeModal.close(this.resForm);
        }
      )
    //construction de la vue
    Survey.SurveyNG.render("surveyElement", { model: this.surveyModel/*, css: this.myCss*/ });//lien avec l'id du html et fait la vue
  }
}
