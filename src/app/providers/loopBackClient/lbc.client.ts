import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { LoopbackModel } from "./lbc.model";

import { environment } from '../../../environments/environment';
import { CookieService } from 'ngx-cookie-service';


@Injectable()

export class LoopbackClient {

    baseUrl = environment.apiUrl
    token: string;
    headers = {};

    constructor(public http: HttpClient, private cookieService: CookieService) {

        const token = this.cookieService.get('loopBackClientToken')

        if (token) {
            console.log('token=', token)
            this.token = token;
        };

    }

    getBaseUrl() {
        return this.baseUrl;
    }

    getToken() {
        return this.token;
    }

    getModel(name: string) {
        return new LoopbackModel(name, this);
    }

    login(email: string, password: string): Promise<any> {
        const data = {
            email: email,
            password: password
        };

        return new Promise(resolve => {

            this.http
                .post(this.baseUrl + "/user/login?include=user", data)
                .subscribe(async data => {
                    this.token = (data as any).id;
                    await this.cookieService.set('loopBackClientToken', this.token);
                    resolve(data);
                });
        });
    }
}