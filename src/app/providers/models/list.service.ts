import { Injectable } from '@angular/core';

import { ModelService } from './model.service'

@Injectable()
export class ListService extends ModelService {

    constructor() {
        super('List');
    }

}