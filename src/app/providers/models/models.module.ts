import { NgModule } from "@angular/core";
import { LbcModule } from "../loopBackClient/lbc.module";
import { Injector } from "@angular/core";
import { setAppInjector } from "./model.injector";
import { ListService } from './list.service';
import { SurveyService } from "./survey.service";
import { surveyModelService } from "./surveyModel.service";


@NgModule({
  imports: [LbcModule],
  exports: [],
  providers: [
   ListService,SurveyService,surveyModelService
  ]
})
export class ModelsModule {
  constructor(injector: Injector) {
    setAppInjector(injector);
  }
}
