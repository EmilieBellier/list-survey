import { Injectable } from '@angular/core';

import { ModelService } from './model.service'

@Injectable()
export class surveyModelService extends ModelService {

    constructor() {
        super('surveyModel');
    }

}