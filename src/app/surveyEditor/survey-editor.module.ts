import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { NgJsonEditorModule } from 'ang-jsoneditor' 

import { SurveyEditorComponent } from './survey-editor/survey-editor.component';
import { SurveyEditorService } from './survey-editor.service';
import { NgHttpLoaderModule } from 'ng-http-loader/ng-http-loader.module';



@NgModule({
  imports: [
    CommonModule,
    NgJsonEditorModule,
    NgHttpLoaderModule,
    HttpClientModule
  ],
  declarations: [ SurveyEditorComponent ],
  providers: [ SurveyEditorService ],
  exports:[ SurveyEditorComponent ],
  schemas : [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class SurveyEditorModule { }
